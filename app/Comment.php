<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    public $timestamps = false;

    protected $fillable = ['texto', 'post_id'];

    protected $guarded = ['id'];

    protected $table = 'comments';

    public function posts()
    {
      return $this->belongsTo('App\Post', 'post_id');
    }
}
