<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    public $timestamps = false;

    protected $fillable = ['nome'];

    protected $guarded = ['id'];

    protected $table = 'posts';

    public function comments()
    {
      return $this->hasMany('App\Comment');
    }
}
