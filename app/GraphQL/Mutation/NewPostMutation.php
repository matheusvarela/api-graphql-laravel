<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use App\Post;
use DB;

/**
 *
 */
class NewPostMutation extends Mutation
{

  protected $attributes = [
    'name' => 'NewPost'
  ];

  public function type()
  {
    return GraphQL::type('posts');
  }

  public function args()
  {
    return [
      'nome' => [
        'name' => 'nome',
        'type' => Type::nonNull(Type::string())
      ]
    ];
  }

  public function resolve($root, $args)
  {
    $post = Post::create($args);
    if (!$post) {
      return null;
    }

    return $post;

  }
}


 ?>
