<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use App\Comment;
use App\Post;
use DB;

/**
 *
 */
class NewCommentMutation extends Mutation
{

  protected $attributes = [
    'name' => 'NewComment'
  ];

  public function type()
  {
    return GraphQL::type('comments');
  }

  public function args()
  {
    return [
      'texto' => [
        'name' => 'texto',
        'type' => Type::nonNull(Type::string())
      ],
      'post_id' => [
        'name' => 'post_id',
        'type' => Type::nonNull(Type::int())
      ]
    ];
  }

  public function resolve($root, $args)
  {
    $post = Post::findOrFail($args['post_id']);

    if (!$post) {
      return null;
    }

    $comment = new Comment();

    $comment->texto = $args['texto'];
    $comment->post_id = $args['post_id'];

    $comment->save();

    return $comment;

  }
}


 ?>
