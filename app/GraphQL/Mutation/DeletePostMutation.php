<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use App\Post;
use DB;

/**
 *
 */
class DeletePostMutation extends Mutation
{

  protected $attributes = [
    'name' => 'DeletePost'
  ];

  public function type() {
    return GraphQL::type('posts');
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::nonNull(Type::int())
      ]
    ];
  }

  public function resolve($root, $args)
  {
    $post = Post::findOrFail($args['id']);

    if (!$post) {
      return null;
    }

    $post->delete();

  }
}


 ?>
