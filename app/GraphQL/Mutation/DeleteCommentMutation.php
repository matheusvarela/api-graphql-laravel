<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use App\Comment;
use DB;

/**
 *
 */
class DeleteCommentMutation extends Mutation
{

  protected $attributes = [
    'name' => 'DeleteComment'
  ];

  public function type()
  {
    return GraphQL::type('comments');
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::nonNull(Type::int())
      ]
    ];
  }

  public function resolve($root, $args)
  {
    $comment = Comment::findOrFail($args['id']);

    if (!$comment) {
      return null;
    }

    $comment->delete();
  }
}


 ?>
