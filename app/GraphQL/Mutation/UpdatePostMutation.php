<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use App\Post;
use DB;

/**
 *
 */
class UpdatePostMutation extends Mutation
{

  protected $attributes = [
    'name' => 'UpdatePost'
  ];

  public function type()
  {
    return GraphQL::type('posts');
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::nonNull(Type::int())
      ],
      'name' => [
        'name' => 'nome',
        'type' => Type::nonNull(Type::string())
      ]
    ];
  }

  public function resolve($root, $args)
  {

    $post = Post::find($args['id']);
    if (!$post) {
      return null;
    }

    $post->nome = $args['nome'];
    $post->save();

    return $post;
  }
}


 ?>
