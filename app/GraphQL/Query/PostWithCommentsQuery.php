<?php

namespace App\GraphQL\Query;
use DB;
use App\Post;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

/**
 *
 */
class PostWithCommentsQuery extends Query
{

  protected $attributes = [
    'name' => 'Post',
    'description' => 'All posts with comments'
  ];

  public function type()
  {
    return Type::listOf(GraphQL::type('posts'));
  }

  public function resolve()
  {
    return Post::has('comments')->get();
  }
}


 ?>
