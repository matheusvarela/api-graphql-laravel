<?php

namespace App\GraphQL\Query;
use DB;
use App\Post;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

/**
 *
 */
class PostOneAllCommentsQuery extends Query
{

  protected $attributes = [
    'name' => 'Post',
    'description' => 'One Post All Comments'
  ];

  public function type()
  {
    return Type::listOf(GraphQL::type('posts'));
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::int()
      ],
      'nome' => [
        'name' => 'nome',
        'type' => Type::string()
      ]
    ];
  }

  public function resolve($root, $args, SelectFields $fields)
  {
    $where = function($query) use ($args) {
      if (isset($args['id'])) {
        $query->where('id', $args['id']);
      }

      if (isset($args['nome'])) {
        $query->where('nome', 'like', '%'.$args['nome'].'%');
      }
    };
      $with = array_keys($fields->getRelations());
      return Post::with($with)->where($where)->select($fields->getSelect())->paginate();
    }
}


 ?>
