<?php

namespace App\GraphQL\Query;
use DB;
use App\Comment;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 */
class CommentsQuery extends Query
{

  protected $attributes = [
    'name' => 'Comment',
    'description' => 'A Comment'
  ];

  public function type()
  {
    return Type::listOf(GraphQL::type('comments'));
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::int()
      ],
      'texto' => [
        'name' => 'texto',
        'type' => Type::string()
      ],
      'post_id' => [
        'name' => 'post_id',
        'type' => Type::int()
      ]
    ];
  }

  public function resolve($root, $args, $context, ResolveInfo $info)
  {
    //return DB::select('exec select_comments');

    $fields = $info->getFieldSelection($depth = 4);

        $comments = Comment::query();

        foreach ($fields as $field => $keys) {
            if ($field === 'posts') {
                $comments->with('posts');
            }

        }

        return $comments->get();
  }
}


 ?>
