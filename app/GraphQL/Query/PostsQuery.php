<?php

namespace App\GraphQL\Query;
use DB;
use App\Post;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
/**
 *
 */
class PostsQuery extends Query
{


  protected $attributes = [
    'name' => 'Post',
    'description' => 'A Post'
  ];

  public function type()
  {
    return Type::listOf(GraphQL::type('posts'));
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::int()
      ],
      'nome' => [
        'name' => 'nome',
        'type' => Type::string()
      ]
    ];
  }

  public function resolve($root, $args, $context, ResolveInfo $info)
  {
    //return DB::select('exec select_posts');
    $fields = $info->getFieldSelection($depth = 3);

        $posts = Post::query();

        foreach ($fields as $field => $keys) {
            if ($field === 'comments') {
                $posts->with('comments');
            }

        }

        return $posts->get();
  }
}


 ?>
