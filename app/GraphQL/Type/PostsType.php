<?php

namespace App\GraphQL\Type;

use App\Post;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

/**
 *
 */
class PostsType extends GraphQLType
{

  protected $attributes = [
    'name' => 'Posts',
    'description' => 'A Type',
    'model' => Post::class,
  ];

  public function fields()
  {
    return [
      'id' => [
        'type' => Type::nonNull(Type::int()),
        'description' => 'The id of the post'
      ],
      'nome' => [
        'type' => Type::string(),
        'description' => 'The name of the post'
      ],
      'comments' => [
        'type' => Type::listOf(GraphQL::type('comments')),
        'description' => 'Comments of the post',
      ]
    ];
  }
}


 ?>
