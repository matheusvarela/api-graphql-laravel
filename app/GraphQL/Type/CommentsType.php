<?php

namespace App\GraphQL\Type;

use App\Comment;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

/**
 *
 */
class CommentsType extends GraphQLType
{

  protected $attributes = [
    'name' => 'Comments',
    'description' => 'A Type',
    'model' => Comment::class,
  ];

  public function fields()
  {
    return [
      'id' => [
        'type' => Type::nonNull(Type::int()),
        'description' => 'The id of the comment'
      ],
      'texto' => [
        'type' => Type::string(),
        'description' => 'The texto of the comment'
      ],
      'post_id' => [
        'type' => Type::nonNull(Type::int()),
        'description' => 'The id of the post'
      ],
      'posts' => [
        'type' => GraphQL::type('posts'),
        'description' => 'The post_id of the post'
      ]
    ];
  }
}

 ?>
