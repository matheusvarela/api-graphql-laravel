## Pacotes que foram usados na aplicação

Pacote GraphQL/Laravel - Acessar o link [Click aqui](https://github.com/rebing/graphql-laravel)


Pacote Xethron, que gera migrações apartir do banco para aplicação. Acessar o link [Click aqui](https://blog.especializati.com.br/criar-migrations-no-laravel-a-partir-de-banco-de-dados-existente/)

Foi seguido as instruções fornecidas em ambos os links.

## Para executar a aplicação
>  Esteja no diretorio do projeto

Execute o comando `php artisan serve`

Após isso abra seu navegador na interface do graphiql
`http://localhost:8000/graphiql`

Para acessar as query e mutation do GraphQL, basta acessar a pasta arquivos, no diretorio `api-graphql-laravel/arquivos/schema_graphql.graphql`

## Usando postman para rodar a API

> GET

http://localhost:8000/graphql?query={adicione aqui a query entre essas chaves}

Exemplo: `http://localhost:8000/graphql?query={postWithComments {
    id
    nome
    comments {
      id
      texto
      post_id
    }
  }}`

> POST

http://localhost:8000/graphql?

Selecione `raw` e o tipo `JSON(application/json)`

New Post com nome "Post"
Exemplo: `{"query": "mutation { newPost(nome: \"Post\") { nome } }"}`

Delete Post Id 1
Exemplo: `{"query": "mutation { deletePost(id: 1) { id nome } } "}`

Update Post Id 1
Exemplo `{"query": "mutation { updatePost(id: 1, nome: \"Testando Update\") { id nome } }"}`