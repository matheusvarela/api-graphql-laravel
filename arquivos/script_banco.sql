/********************************************* SCRIPT COMPLETO **********************************************************************/
/***********************************************************************************************************************************/

CREATE DATABASE blog;
go
use blog;
go
/********************************************** CREATE TABLES ******************************************************************/
/******************************************************************************************************************************/

create table posts (
		id int not null primary key identity,
		nome varchar(100) not null
)
create table comments(
	id int not null primary key identity,
	texto text not null,
	post_id int not null,
	foreign key (post_id) references posts(id) on delete cascade
)
go
/*********************************************** PROCEDURES POSTS ************************************************************/
/****************************************************************************************************************************/
/*
	INSERT POSTS
*/
create procedure insert_posts
	@nome varchar(50)
as
begin try
	SET NOCOUNT ON;
	insert into posts(nome)
	values (@nome);
end try
begin catch
		select
		ERROR_MESSAGE() as msg,
		ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	UPDATE POSTS
*/
create procedure update_posts
      @nome varchar(50),
      @id int
as
begin try
      UPDATE posts SET
      nome=@nome
      WHERE posts.id=@id
end try

begin catch
        select
              ERROR_MESSAGE() as msg,
              ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	DELETE POSTS
*/
create procedure delete_posts
      @id int
as
begin try
	DELETE FROM posts
	FROM comments
	INNER JOIN posts ON comments.post_id = @id WHERE posts.id = @id
end try

begin catch
        select
              ERROR_MESSAGE() as msg,
              ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	SELECT POSTS
*/
create procedure select_posts
as
begin try
	select * from posts
end try

begin catch
	select
			ERROR_MESSAGE() as msg,
			ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	SELECT ONE POST AND ALL COMMENTS
*/
create procedure select_one_post_comments
		@id int
as
begin try
	select * from posts inner join
	comments on
	posts.id = @id and comments.post_id = @id
end try

begin catch
		select
				ERROR_MESSAGE() as msg,
				ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	SELECT ALL POSTS WITH COMMENTS
*/
create procedure select_posts_with_comments
as
begin try
	SELECT DISTINCT posts.id, nome FROM posts INNER JOIN comments on posts.id = comments.post_id
end try

begin catch
		select
				ERROR_MESSAGE() as msg,
				ERROR_PROCEDURE() as aProcedure
end catch;
go
/*********************************************** PROCEDURES COMMENTS *********************************************************/
/****************************************************************************************************************************/

/*
	INSERT COMMENTS
*/
create procedure insert_comments
      @texto text,
      @post_id int
as
begin try
		SET NOCOUNT ON;
		insert into comments(comments.texto, comments.post_id)
		SELECT @texto, @post_id WHERE EXISTS (SELECT 1 FROM posts WHERE posts.id=@post_id)

end try

begin catch
		select
				ERROR_MESSAGE() as msg,
				ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	UPDATE COMMENTS
*/
create procedure update_comments
      @texto text,
      @id int,
	  @post_id int
as
begin try
      UPDATE comments SET
      texto=@texto
      WHERE comments.id=@id and comments.post_id=@post_id
end try

begin catch
        select
              ERROR_MESSAGE() as msg,
              ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	DELETE COMMENTS
*/
create procedure delete_comments
      @id int
as
begin try
	DELETE FROM comments WHERE comments.id = @id
end try

begin catch
        select
              ERROR_MESSAGE() as msg,
              ERROR_PROCEDURE() as aProcedure
end catch;
go
/*
	SELECT COMMENTS
*/

create procedure select_comments
as
begin try
	select * from comments;
end try

begin catch
		select
			ERROR_MESSAGE() as msg,
			ERROR_PROCEDURE() as aProcedure
end catch;
go
